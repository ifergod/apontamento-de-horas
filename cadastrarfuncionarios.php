<?php
session_start();

$access_token = $_SESSION['access_token'];
$instance_url = $_SESSION['instance_url'];

if (!isset($access_token) || $access_token == "") {
die("Error - access token missing from session!");
}

if (!isset($instance_url) || $instance_url == "") {
die("Error - instance URL missing from session!");
}

/*Na QUERY é necessário colocar o campo 'ID', pois o SALESFORCE tem um ID padrão que será utilizado em códigos futuros*/
$query = "SELECT  Email__c, Name, Nome_do_Funcion_rio__c, Sobrenome_do_Funcion_rio__c, Senha__c,Id FROM Funcion_rios__c ORDER BY Name";
$url = "$instance_url/services/data/v20.0/query?q=" . urlencode($query);

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER,
array("Authorization: OAuth $access_token"));

$json_response = curl_exec($curl);
curl_close($curl);

$response = json_decode($json_response, true);
?>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/3.3.0/firebase.js"></script>
<script src="JS/authfirebase.js"></script>

<?php foreach ((array) $response['records'] as $record) {

$id = $record['Name'];
$email = $record['Email__c'];
$nomefuncionario = $record['Nome_do_Funcion_rio__c'];
$senha = $record['Senha__c'];
$sobrenomefuncionario = $record['Sobrenome_do_Funcion_rio__c'];
$codigo = $record['Id'];

echo "<script>";
      echo "$(document).ready(function(){";
      echo "firebase";
      echo  ".auth()";
      echo  ".createUserWithEmailAndPassword('";
      echo $email;
      echo "','";
      echo $senha."0')";
      echo  ".then(function () {";
          
        echo "})";
        echo ".catch(function (error) {";
          echo  "console.error(error.code);";
          echo  "console.error(error.message);";
      echo  "});";
echo "});";
echo "</script>";

}?>