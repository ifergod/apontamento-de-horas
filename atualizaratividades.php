<?php
session_start();

$access_token = $_SESSION['access_token'];
$instance_url = $_SESSION['instance_url'];

if (!isset($access_token) || $access_token == "") {
die("Error - access token missing from session!");
}

if (!isset($instance_url) || $instance_url == "") {
die("Error - instance URL missing from session!");
}

/*Na QUERY é necessário colocar o campo 'ID', pois o SALESFORCE tem um ID padrão que será utilizado em códigos futuros*/
$query = "SELECT  Data_Final_Prevista__c, Name, Data_Inicial_Prevista__c, Nome_da_Atividade__c, Projetos__c, 	Quantidade_de_Horas_Previstas__c, Id FROM Atividades__c ORDER BY Name";
$url = "$instance_url/services/data/v20.0/query?q=" . urlencode($query);

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER,
array("Authorization: OAuth $access_token"));

$json_response = curl_exec($curl);
curl_close($curl);

$response = json_decode($json_response, true);


?>

  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<script src="https://www.gstatic.com/firebasejs/3.3.0/firebase.js"></script>
<script src="JS/authfirebase.js"></script>



<script>
var database = firebase.database();

var ref = database.ref('Atividades');
ref.on('value', gotData);

function gotData(data){
var scores = data.val();
var keys = Object.keys(scores);

var array = new Array(keys.length);

<?php foreach ((array) $response['records'] as $record) {

$id = $record['Name'];
$datafinalprev = $record['Data_Final_Prevista__c'];
$datainicialprev = $record['Data_Inicial_Prevista__c'];
$nomeatividade = $record['Nome_da_Atividade__c'];
$projetos = $record['Projetos__c'];
$quantidadehorasprev = $record['Quantidade_de_Horas_Previstas__c'];
$codigo = $record['Id'];
?>



for(var i=0, c=0; i < keys.length; i++){
var k = keys[i];
array[i] = new Array(keys.length);
array[i] = scores[k].codigo;
var num = <?php echo "".$id.""; ?>;
if (array[i] == num){
   c += 1;

        $(document).ready(function () {
    <?php echo "create('".$datafinalprev."', '".$datainicialprev."','".$nomeatividade."','".$projetos."','".$quantidadehorasprev."','".$id."','".$codigo."');";?>
});

// Função para criar um registro no Firebase
function create(email,nome,senha,id,sobrenome) {
    var data = {
        id: <?php echo "'".$codigo."'," ;?>
        codigoAtividade: <?php echo "'".$id."'," ;?>
        codigoProjeto: <?php echo "'".$projetos."',";?>
        dataPrevistaFinal: <?php echo "'".$datafinalprev."'," ;?>
        dataPrevistaInicial: <?php echo "'".$datainicialprev."'," ;?>
        nomeAtividade: <?php echo "'".$nomeatividade."',";?>
        quantidadeHorasPrevistas: <?php echo "'".$quantidadehorasprev."'";?>

    };

    /*return firebase.database().ref().child('Atividade').push(data);*/

    firebase.database().ref().child("Atividades").child(<?php echo "'".$id."'";?>).set(data);
   
}

  i=keys.length;
  }
}
if (c == 0){
  console.log(num);
         $(document).ready(function () {
    <?php echo "create('".$datafinalprev."', '".$datainicialprev."','".$nomeatividade."','".$projetos."','".$quantidadehorasprev."','".$id."','".$codigo."');";?>
});

// Função para criar um registro no Firebase
function create(email,nome,senha,id,sobrenome) {
    var data = {
        id: <?php echo "'".$codigo."'," ;?>
        codigoAtividade: <?php echo "'".$id."'," ;?>
        codigoProjeto: <?php echo "'".$projetos."',";?>
        dataPrevistaFinal: <?php echo "'".$datafinalprev."'," ;?>
        dataPrevistaInicial: <?php echo "'".$datainicialprev."'," ;?>
        nomeAtividade: <?php echo "'".$nomeatividade."',";?>
        quantidadeHorasPrevistas: <?php echo "'".$quantidadehorasprev."'";?>

    };

    /*return firebase.database().ref().child('Atividade').push(data);*/

     firebase.database().ref().child("Atividades").child(<?php echo "'".$id."'";?>).set(data);
    
}
  }
<?php } ?>
};
</script>

<script>

var database = firebase.database();

var ref = database.ref('Atividades');
ref.on('value', gotDate);

function gotDate(data){
var scores = data.val();
var keys = Object.keys(scores);

var array = new Array(keys.length);

for(var i=0, c=0; i < keys.length; i++, c=0){
var k = keys[i];
array[i] = scores[k].codigoAtividade;

<?php 
foreach ((array) $response['records'] as $record) {
$id = $record['Name'];
echo "if(".$id."==array[i]){";
  echo "c += 1;";
  echo "console.log(array[i]);
  console.log(".$id.");
}";
}
?>

if(c == 0){
return firebase.database().ref().child("Atividades").child(array[i]).remove();
}

}
}


</script>

