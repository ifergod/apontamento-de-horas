<?php
	session_start();
  $cURL = curl_init('https://apontamento-de-horas-997bf.firebaseio.com/ApontamentoHoras.json');
  curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($cURL, CURLOPT_ENCODING, "UTF-8" );
  $resultado = curl_exec($cURL);
    curl_close($cURL);


    $response = json_decode($resultado, true);
    
    foreach ($response as $key => $value) {
        foreach ($value as $key2 => $value2) {
        	if($key2 == 'nomeAtividade'){
              $nomeAtividade = $value2;
            }
          	if($key2 == "idApontamento"){ 	
          	$idApontamento = $value2;
          	}
      		if($key2 == "dataInicial"){
          	$dataInicial = $value2;
          	$dataInicial = strtotime($dataInicial);
            $dataInicial = strtotime('+3 hours',$dataInicial);
            $dataInicial = date('Y-m-d H:i:s',$dataInicial);
          	$dataInicial = substr($dataInicial, 0,10)."T".substr($dataInicial,11,8)."Z";
          	}
          	if($key2 == "dataFinal"){
          	$dataFinal = $value2;
          	$dataFinal = strtotime($dataFinal);
            $dataFinal = strtotime('+3 hours',$dataFinal);
            $dataFinal = date('Y-m-d H:i:s',$dataFinal);
          	$dataFinal = substr($dataFinal, 0,10)."T".substr($dataFinal,11,8)."Z";
          	}
          	if($key2 == "nomeFuncionario"){
          	$nomeFuncionario = $value2;
          	}
          	if($key2 == "nomeProjeto"){
          	$nomeProjeto = $value2;
          	}

    }
    					

						$access_token = $_SESSION['access_token'];
						$instance_url = $_SESSION['instance_url'];

						if (!isset($access_token) || $access_token == "") {
						die("Error - access token missing from session!");
						}

						if (!isset($instance_url) || $instance_url == "") {
						die("Error - instance URL missing from session!");
						}

						/*Na QUERY é necessário colocar o campo 'ID', pois o SALESFORCE tem um ID padrão que será utilizado em códigos futuros*/
						$query = "SELECT idApontamento__c FROM Apontamento_de_Horas__c ORDER BY Name";
						$url = "$instance_url/services/data/v20.0/query?q=" . urlencode($query);

						$curl = curl_init($url);
						curl_setopt($curl, CURLOPT_HEADER, false);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($curl, CURLOPT_HTTPHEADER,
						array("Authorization: OAuth $access_token"));

						$json_response = curl_exec($curl);
						curl_close($curl);

						$response = json_decode($json_response, true);
						$c = 0;
						foreach ((array) $response['records'] as $record) {
									$idApont = $record['idApontamento__c'];
									if($idApontamento == $idApont){
										$c++;
									}
						        }
						        if($c == 0){

						$url = "$instance_url/services/data/v20.0/sobjects/Apontamento_de_Horas__c/";

					    $content = json_encode(array("idApontamento__c" => $idApontamento, "Funcion_rio__c" => $nomeFuncionario, "Atividade__c" => $nomeAtividade, "Data_Inicial__c" => "$dataInicial", "Data_Final__c" => $dataFinal, "Projeto__c" => $nomeProjeto));

					    $curl = curl_init($url);
					    curl_setopt($curl, CURLOPT_HEADER, false);
					    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					    curl_setopt($curl, CURLOPT_HTTPHEADER,
					            array("Authorization: OAuth $access_token",
					                "Content-type: application/json"));
					    curl_setopt($curl, CURLOPT_POST, true);
					    curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

					    $json_response = curl_exec($curl);

					    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

					    if ( $status != 201 ) {
					        die("'Error: call to URL $url failed with status $status, response $json_response, curl_error '" . curl_error($curl) . "', curl_errno '" . curl_errno($curl));
					    }
					    
					    echo "HTTP status $status creating account<br/><br/>";

					    curl_close($curl);

					    $response = json_decode($json_response, true);

						        }

  }
?>