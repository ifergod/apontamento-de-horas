<?php
session_start();

$access_token = $_SESSION['access_token'];
$instance_url = $_SESSION['instance_url'];

if (!isset($access_token) || $access_token == "") {
die("Error - access token missing from session!");
}

if (!isset($instance_url) || $instance_url == "") {
die("Error - instance URL missing from session!");
}

/*Na QUERY é necessário colocar o campo 'ID', pois o SALESFORCE tem um ID padrão que será utilizado em códigos futuros*/
$query = "SELECT Nome_do_Projeto__c, Id, Name FROM Projetos__c ORDER BY Name";
$url = "$instance_url/services/data/v20.0/query?q=" . urlencode($query);

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER,
array("Authorization: OAuth $access_token"));

$json_response = curl_exec($curl);
curl_close($curl);

$response = json_decode($json_response, true);


?>
<!DOCTYPE>
<html>
<head>
  <title>Firebase</title>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>

<ul id="usersList"></ul>
<script src="https://www.gstatic.com/firebasejs/3.3.0/firebase.js"></script>
<script src="JS/authfirebase.js"></script>



<script>
var database = firebase.database();

var ref = database.ref('Projetos');
ref.on('value', gotData);

function gotData(data){
var scores = data.val();
var keys = Object.keys(scores);
var array = new Array(keys.length);

<?php foreach ((array) $response['records'] as $record) {

$id = $record['Name'];
$id2 = $record['Id'];
$projeto = $record['Nome_do_Projeto__c'];

?>



for(var i=0, c=0; i < keys.length; i++){
var k = keys[i];
array[i] = new Array(keys.length);
array[i] = scores[k].codigoProjeto;
var num = <?php echo "".$id.""; ?>;
if (array[i] == num){
   c += 1;

        $(document).ready(function () {
    <?php echo "create('".$projeto."','".$id."','".$id2."');";?>
});

// Função para criar um registro no Firebase
function create(codigoProjeto,nomeProjeto) {
    var data = {
        codigoProjeto: <?php echo "'".$id2."'," ;?>
        nomeProjeto: <?php echo "'".$projeto."',";?>
        id: <?php echo "'".$id."'" ;?>
    };

    /*return firebase.database().ref().child('Atividade').push(data);*/

    return firebase.database().ref().child("Projetos").child(<?php echo "'".$id2."'";?>).set(data);
}

  i=keys.length;
  }
}
if (c == 0){
  console.log(num);
         $(document).ready(function () {
    <?php echo "create('".$projeto."','".$id."','".$id2."');";?>
});

// Função para criar um registro no Firebase
function create(codigoProjeto,nomeProjeto) {
    var data = {
        codigoProjeto: <?php echo "'".$id2."'," ;?>
        nomeProjeto: <?php echo "'".$projeto."',";?>
        id: <?php echo "'".$id."'" ;?>
    };

    /*return firebase.database().ref().child('Atividade').push(data);*/

    return firebase.database().ref().child("Projetos").child(<?php echo "'".$id."'";?>).set(data);
}
  }
<?php } ?>
};
</script>


<!-- -------------------------------------------------------------------------------------------------------------- -->

<script>

var database = firebase.database();

var ref = database.ref('Projetos');
ref.on('value', gotDate);

function gotDate(data){
var scores = data.val();
var keys = Object.keys(scores);
console
var array = new Array(keys.length);
console.log(scores);

for(var i=0, c=0; i < keys.length; i++, c=0){
var k = keys[i];
array[i] = scores[k].id;



<?php 
foreach ((array) $response['records'] as $record) {
$id = $record['Name'];
echo "if(".$id."==array[i]){";
  echo "c += 1;";
  echo "
}";
}
?>

if(c == 0){
return firebase.database().ref().child("Projetos").child(array[i]).remove();
}

}
}


</script>


</body>
</html>